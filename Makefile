CC=g++

# C flags
# with all warnings, with extra warnings
CFLAGS=
# C preprocess flags
CPPFLAGS = -DDEBUG -Wall -Wextra -std=c++14 -g

# -Wl,(XX) means send xx to linker
# -Wl,--gc-sections means send --gc-sections to linker
LDFLAGS=-Wl,--gc-sections -g

# the first target in makefile is the default target
# which means
# `make` is the same with `make all`

# target: dependencies
# the next line of target starts with tab

executable_name := main

TOP  := $(shell pwd)/
SRC := .
all_source_files = $(sort $(shell find $(SRC) -name "*.cc"))

$(executable_name): $(all_source_files:.cc=.o)
	$(CC) $(LDFLAGS) $^ -o $(executable_name)


# $@ target
# $< first prerequisite
# $D the directory part of $@
# $^ The names of all the prerequisites, with spaces between them
# https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html

-include $(all_source_files:.cc=.d)

%.d: %.cc
	set -e; rm -f $@; \
	$(CC) -MM $(CPPFLAGS) $< > $@.$$$$; \
	sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$


# A phony target is one that is not really the name of a file
.PHONY: clean
clean:
	rm -f exectable_name
	rm -f *.o
	rm -f *.d
