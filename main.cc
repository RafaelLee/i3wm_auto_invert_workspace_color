// #include <stdio.h>

// #include "main.hh"

// int my_add (int a, int b);

// int main (void)
// {
//   printf ("%d\n", my_add (1, 2));
//   return 0;
// }



// http://www.cplusplus.com/reference/cstdlib/system/
// /* system example : DIR */
// #include <stdio.h>      /* printf */
// #include <stdlib.h>     /* system, NULL, EXIT_FAILURE */

// int main ()
// {
//   int i;
//   printf ("Checking if processor is available...");
//   if (system(NULL)) puts ("Ok");
//   else exit (EXIT_FAILURE);
//   printf ("Executing command DIR...\n");
//   i=system ("ls -l");
//   printf ("The value returned was: %d.\n",i);
//   return 0;
// }

// https://stackoverflow.com/questions/478898/how-do-i-execute-a-command-and-get-output-of-command-within-c-using-posix
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>

using namespace std;

std::string exec (const char *cmd)
{
  std::array<char, 4096> buffer;
  std::string result;
  std::unique_ptr<FILE, decltype (&pclose)> pipe (popen (cmd, "r"), pclose);
  if (!pipe)
  {
    throw std::runtime_error ("popen() failed!");
  }
  while (fgets (buffer.data(), buffer.size(), pipe.get()) != nullptr)
  {
    result += buffer.data();
  }
  return result;
}

#include <unistd.h>

std::string filter (std::string input)
{

  ssize_t iter = 0;
  ssize_t size = input.size();
  ssize_t num_pos = 0;
  ssize_t end = 0;
  const std::string true_str ("true");
  const std::string num_str ("num");
  const std::string empty ("empty");


  // cout << input << endl;
  while (1)
  {
    iter = input.find ("\"focused\":", iter + 1);
    if (std::string::npos == iter)
    {
      return empty;
    }

    // cout << input.substr (iter) << endl;

    if (0 == input.compare (iter + 10, 4, "true"))
    {
      // cout << "!!!!!!!!" << endl;
      num_pos = input.rfind ("num", iter);
      num_pos = input.find (":", num_pos);
      num_pos++;
      // cout << input.substr (num_pos, input.find (",", num_pos-7) ) << endl;
      end = input.find (",", num_pos);
      // printf("num_pos = %d, end=%d\n",num_pos,end);
      std::string result (input.substr (num_pos, end - num_pos));
      // cout << result << endl;
      return result;
    }
    else
    {
      // cout << "^^^^^^^^" << endl;
    }
  }
}


int main()
{
  // char batcmd[] = "i3-msg -t get_workspaces|jq '.[]| select(.focused==true).name'|cut -d \\\"  -f2 ";
  char batcmd2[] = "i3-msg -t get_workspaces";

  // char batcmd[] =" \" ";
  // cout << batcmd << endl;

  std::string last_result;
  // std::string  result_string=exec (batcmd);
  std::string result;

  while (1)
  {
    result = filter (exec (batcmd2));

    if ("4" == result && (result != last_result))
    {
      exec ("xcalib -a -i");
      // cout << exec ("xcalib -a -i") << endl;
      last_result = result;
    }
    else if (result != last_result)
    {
      exec ("xcalib -c");
      last_result = result;
    }

    else
    {
    }
    usleep (50000);

  }
}
